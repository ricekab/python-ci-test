# python-ci-test

Test Gitlab CI/CD project for a generic Python package.

## Description

This repo is meant as an exercise to perform common Python CI/CD actions.

Informal checklist:

- [X] flake8 lint
- [X] run pytest cases
- [X] Test build wheel and source distributions
- [ ] Upload build to PyPI.org (main / release only)
- [ ] Generate sphinx documentation
- [ ] Upload sphinx documentation to readthedocs (main / release only)

## Installation

TODO

## Usage

TODO

## License

This repository is MIT licensed.
