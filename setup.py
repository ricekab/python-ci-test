from setuptools import setup, find_packages

__about__ = {}
with open("python/ricekabpythonci/__about__.py") as fp:
    exec(fp.read(), __about__)

setup(
    name=__about__['__name__'],
    version=__about__['__version__'],
    description=__about__['__description__'],
    url='https://gitlab.com/ricekab/python-ci-test',
    author='Kevin Chi Yan Tang',
    author_email='contact@kevinchiyantang.com',
    packages=find_packages(where='python',
                           exclude=['tests']),
    package_dir={'': 'python'},
    package_data={"ricekabpythonci": ["resources/*"]},
    license='MIT',
    install_requires=[],
    tests_require=['pytest==7.*',
                   'flake8==4.*',
                   'radon==5.*',
                   ],
    python_requires='>=3.8',
    classifiers=[
        'Development Status :: 1 - Planning',
        'Intended Audience :: Developers',
        'Topic :: Software Development',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3'
    ],
    # entry_points='''
    #     [console_scripts]
    #     my-cmd=ricekabpythonci.cli:cli_entry
    # '''
)
