import string

from ricekabpythonci.simple import generate_random_string


def test_generate_random_string_numbers_only():
    for _ in range(10):
        generated = generate_random_string(length=8,
                                           valid_characters=string.digits)
        assert len(generated) == 8
        assert all(char_ in string.digits for char_ in generated)


def test_generate_random_string_ascii_characters():
    for _ in range(10):
        generated = generate_random_string(length=8,
                                           valid_characters=string.ascii_letters)
        assert len(generated) == 8
        assert all(char_ in string.ascii_letters for char_ in generated)


def test_generate_random_string_character_list():
    valid_chars = "~-/=.()"
    for _ in range(10):
        generated = generate_random_string(length=10,
                                           valid_characters=valid_chars)
        assert len(generated) == 10
        assert all(char_ in valid_chars for char_ in generated)
