import string
import random


def generate_random_string(length: int,
                           valid_characters: list = string.ascii_letters):
    # Missing import should raise error -> lint + test error
    return "".join(random.choices(valid_characters, k=length))

# This very long comment should no longer be flagged as a lint issue.
